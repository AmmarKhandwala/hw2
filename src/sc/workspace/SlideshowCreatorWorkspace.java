package sc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import sc.SlideshowCreatorApp;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import static sc.SlideshowCreatorProp.ADD_ALL_IMAGES_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ADD_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.CAPTION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.MOVE_DOWN_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.MOVE_UP_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.PATH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.REMOVE_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.SLIDESHOW_TITLE_TEXT;
import static sc.SlideshowCreatorProp.UPDATE_BUTTON_TEXT;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_BUTTON;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_SLIDER;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.SlideshowCreatorStyle.CLASS_PROMPT_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SLIDES_TABLE;
import static sc.style.SlideshowCreatorStyle.CLASS_UPDATE_BUTTON;

/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna
 * @co-author Ammar Khandwala 109980256
 */
public class SlideshowCreatorWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    SlideshowCreatorApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    SlideshowCreatorController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    Button addAllImagesInDirectoryButton;
    Button addImageButton;
    Button removeImageButton;
    Button upButton;
    Button downButton;

    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> fileNameColumn;
    TableColumn<Slide, IntegerProperty> currentWidthColumn;
    TableColumn<Slide, IntegerProperty> currentHeightColumn;
    TableColumn<Slide, IntegerProperty> XColumn;
    TableColumn<Slide, IntegerProperty> YColumn;

    // THE EDIT PANE
    GridPane editPane;
    Label fileNamePromptLabel;
    TextField slideshowTitle;
    TextField fileNameTextField;
    Label pathPromptLabel;
    TextField pathTextField;
    Label captionPromptLabel;
    TextField captionTextField;
    Label originalWidthPromptLabel;
    TextField originalWidthTextField;
    Label originalHeightPromptLabel;
    TextField originalHeightTextField;
    Label currentWidthPromptLabel;
    Slider currentWidthSlider;
    Label currentHeightPromptLabel;
    Slider currentHeightSlider;
    Button updateButton;
    Slider xPosition;
    Label xPositionPrompLabel;
    Slider yPosition;
    Label yPositionPromptLabel;

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public SlideshowCreatorWorkspace(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
    
    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
        editImagesToolbar = new HBox();
        addAllImagesInDirectoryButton = new Button(props.getProperty(ADD_ALL_IMAGES_BUTTON_TEXT));
        addImageButton = new Button(props.getProperty(ADD_IMAGE_BUTTON_TEXT));
        removeImageButton = new Button(props.getProperty(REMOVE_IMAGE_BUTTON_TEXT));
        upButton = new Button(props.getProperty(MOVE_UP_BUTTON_TEXT));
        downButton = new Button(props.getProperty(MOVE_DOWN_BUTTON_TEXT));
        slidesTableScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        XColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        YColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        editPane = new GridPane();
        fileNamePromptLabel = new Label(props.getProperty(FILE_NAME_PROMPT_TEXT));
        fileNameTextField = new TextField();
        slideshowTitle = new TextField();
        pathPromptLabel = new Label(props.getProperty(PATH_PROMPT_TEXT));
        pathTextField = new TextField();
        captionPromptLabel = new Label(props.getProperty(CAPTION_PROMPT_TEXT));
        captionTextField = new TextField();
        originalWidthPromptLabel = new Label(props.getProperty(ORIGINAL_WIDTH_PROMPT_TEXT));
        originalWidthTextField = new TextField();
        originalHeightPromptLabel = new Label(props.getProperty(ORIGINAL_HEIGHT_PROMPT_TEXT));
        originalHeightTextField = new TextField();
        currentWidthPromptLabel = new Label(props.getProperty(CURRENT_WIDTH_PROMPT_TEXT));
        currentWidthSlider = new Slider();
        currentHeightPromptLabel = new Label(props.getProperty(CURRENT_HEIGHT_PROMPT_TEXT));
        currentHeightSlider = new Slider();
        xPosition = new Slider();
        xPositionPrompLabel = new Label("X Position");
        yPosition = new Slider();
        yPositionPromptLabel = new Label("Y Position");
        updateButton = new Button(props.getProperty(UPDATE_BUTTON_TEXT));

        // ARRANGE THE TABLE
        slideshowTitle.setPromptText(props.getProperty(SLIDESHOW_TITLE_TEXT));
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        XColumn = new TableColumn("X");
        YColumn = new TableColumn("Y");
        slidesTableView.getColumns().add(fileNameColumn);
        slidesTableView.getColumns().add(currentWidthColumn);
        slidesTableView.getColumns().add(currentHeightColumn);
        slidesTableView.getColumns().add(XColumn);
        slidesTableView.getColumns().add(YColumn);
        fileNameColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        currentWidthColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        currentHeightColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        XColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(8));
        YColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(8));
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("fileName")
        );
        currentWidthColumn.setCellValueFactory(
                new PropertyValueFactory<>("currentWidth")
        );
        currentHeightColumn.setCellValueFactory(
                new PropertyValueFactory<>("CurrentHeight")
        );
        XColumn.setCellValueFactory(
                new PropertyValueFactory<>("xPosition")
        );
        YColumn.setCellValueFactory(
                new PropertyValueFactory<>("yPosition")
        );

        //SLIDER UNITS AND LABELS
        currentHeightSlider.setMin(0);
        currentHeightSlider.setMax(1000);
        currentHeightSlider.setMajorTickUnit(200);
        currentHeightSlider.setShowTickLabels(true);
        currentHeightSlider.setBlockIncrement(40);
        currentHeightSlider.setShowTickMarks(true);
        currentWidthSlider.setMin(0);
        currentWidthSlider.setMax(1000);
        currentWidthSlider.setMajorTickUnit(200);
        currentWidthSlider.setBlockIncrement(40);
        currentWidthSlider.setShowTickLabels(true);
        currentWidthSlider.setShowTickMarks(true);
        xPosition.setMin(0);
        xPosition.setMax(1000);
        xPosition.setMajorTickUnit(200);
        xPosition.setBlockIncrement(40);
        xPosition.setShowTickLabels(true);
        xPosition.setShowTickMarks(true);
        yPosition.setMin(0);
        yPosition.setMax(1000);
        yPosition.setMajorTickUnit(200);
        yPosition.setBlockIncrement(40);
        yPosition.setShowTickLabels(true);
        yPosition.setShowTickMarks(true);

        // HOOK UP THE TABLE TO THE DATA
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        slidesTableView.setItems(model);

        // THEM ORGANIZE THEM
        editImagesToolbar.getChildren().add(slideshowTitle);
        editImagesToolbar.getChildren().add(addAllImagesInDirectoryButton);
        editImagesToolbar.getChildren().add(addImageButton);
        editImagesToolbar.getChildren().add(removeImageButton);
        editImagesToolbar.getChildren().add(upButton);
        editImagesToolbar.getChildren().add(downButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(fileNamePromptLabel, 0, 0);
        editPane.add(fileNameTextField, 1, 0);
        editPane.add(pathPromptLabel, 0, 1);
        editPane.add(pathTextField, 1, 1);
        editPane.add(captionPromptLabel, 0, 2);
        editPane.add(captionTextField, 1, 2);
        editPane.add(originalWidthPromptLabel, 0, 3);
        editPane.add(originalWidthTextField, 1, 3);
        editPane.add(originalHeightPromptLabel, 0, 4);
        editPane.add(originalHeightTextField, 1, 4);
        editPane.add(currentWidthPromptLabel, 0, 5);
        editPane.add(currentWidthSlider, 1, 5);
        editPane.add(currentHeightPromptLabel, 0, 6);
        editPane.add(currentHeightSlider, 1, 6);
        editPane.add(xPositionPrompLabel, 0, 7);
        editPane.add(xPosition, 1, 7);
        editPane.add(yPositionPromptLabel, 0, 8);
        editPane.add(yPosition, 1, 8);
        editPane.add(updateButton, 0, 9);

        // DISABLE THE DISPLAY TEXT FIELDS AND BUTTONS
        fileNameTextField.setDisable(true);
        pathTextField.setDisable(true);
        originalWidthTextField.setDisable(true);
        originalHeightTextField.setDisable(true);
        addAllImagesInDirectoryButton.setDisable(true);
        addImageButton.setDisable(true);
        removeImageButton.setDisable(true);
        disableFields(true);

        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(editImagesToolbar);
        BorderPane workspaceBorderPane = new BorderPane();
        workspaceBorderPane.setCenter(slidesTableScrollPane);
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        workspaceBorderPane.setRight(editPane);

        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;
    }
    
    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING

        controller = new SlideshowCreatorController(app);
        
        addAllImagesInDirectoryButton.setOnAction(e -> {
            controller.handleAddAllImagesInDirectory();
            app.getGUI().updateToolbarControls(false);
        });
        
        addImageButton.setOnAction((ActionEvent e) -> {
            controller.addImage();
            slidesTableView.getSelectionModel().selectLast();
            controller.setDetails();
            app.getGUI().updateToolbarControls(false);
        });
        
        updateButton.setOnAction((ActionEvent e) -> {
            controller.updateDetails();
            updateButton.setDisable(true);
            app.getGUI().updateToolbarControls(false);
        });
        
        slidesTableView.setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() >= 1) {
                controller.setDetails();
                disableFields(false);
                
            }
        });
        
        editImagesToolbar.getChildren().get(0).setOnMouseClicked((MouseEvent me) -> {
            if (me.getClickCount() >= 1) {

                //controller.setSlideshowTitle();
            }
            
        });
        
        removeImageButton.setOnAction((ActionEvent e) -> {
            controller.removeImage();
            app.getGUI().updateToolbarControls(false);
        });
        
    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    private void initStyle() {
        editImagesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        addAllImagesInDirectoryButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        addImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        upButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        downButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }
        
        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileNamePromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fileNameTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        pathPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        pathTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        captionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        captionTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalWidthTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalHeightTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        currentWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentWidthSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentHeightSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        xPositionPrompLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        xPosition.getStyleClass().add(CLASS_EDIT_SLIDER);
        yPositionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        yPosition.getStyleClass().add(CLASS_EDIT_SLIDER);
        updateButton.getStyleClass().add(CLASS_UPDATE_BUTTON);
        
    }
    
    @Override
    public void disableFields(Boolean value) {
        updateButton.setDisable(value);
        captionTextField.setDisable(value);
        currentWidthSlider.setDisable(value);
        currentHeightSlider.setDisable(value);
        xPosition.setDisable(value);
        yPosition.setDisable(value);
        
    }
    
    @Override
    public void resetWorkspace() {
        captionTextField.clear();
        pathTextField.clear();
        fileNameTextField.clear();
        originalHeightTextField.clear();
        originalWidthTextField.clear();
        currentWidthSlider.setValue(0);
        currentHeightSlider.setValue(0);
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        addAllImagesInDirectoryButton.setDisable(false);
        addImageButton.setDisable(false);
        removeImageButton.setDisable(false);
        disableFields(true);
    }
    /* Listen for selection */
}
