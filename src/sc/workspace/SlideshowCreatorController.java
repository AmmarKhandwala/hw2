package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import static sc.SlideshowCreatorProp.APP_PATH_WORK;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_MESSAGE;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_TITLE;
import sc.data.SlideshowCreatorData;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna
 * @co-author Ammar Khandwala 109980256
 * @version 1.0
 */
public class SlideshowCreatorController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    SlideshowCreatorApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public SlideshowCreatorController(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    // CONTROLLER METHOD THAT HANDLES ADDING A DIRECTORY OF IMAGES
    public void handleAddAllImagesInDirectory() {
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            DirectoryChooser dirChooser = new DirectoryChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dirChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = dirChooser.showDialog(app.getGUI().getWindow());
            if (dir != null) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    String fileName = f.getName();
                    if (fileName.toLowerCase().endsWith(".png")
                            || fileName.toLowerCase().endsWith(".jpg")
                            || fileName.toLowerCase().endsWith(".gif")) {
                        String path = f.getPath();
                        String caption = "";
                        Image slideShowImage = loadImage(path);
                        int originalWidth = (int) slideShowImage.getWidth();
                        int originalHeight = (int) slideShowImage.getHeight();
                        int xPosition = 0;
                        int yPosition = 0;
                        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                        if ((checkDuplicate(data, fileName, path)) == 0) {
                            data.addSlide(fileName, path, caption, originalWidth, originalHeight, xPosition, yPosition);
                        } else {
                            System.out.println("File already exists");
                        }
                    }
                }
            }
        } catch (MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }

    public void addImage() {

        try {
            FileChooser fileChooser = new FileChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            fileChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File file = fileChooser.showOpenDialog(app.getGUI().getWindow());
            String fileName = file.getName();
            if (fileName.toLowerCase().endsWith(".png")
                    || fileName.toLowerCase().endsWith(".jpg")
                    || fileName.toLowerCase().endsWith(".gif")) {
                String path = file.getPath();
                String caption = "";
                Image slideShowImage = loadImage(path);
                int originalWidth = (int) slideShowImage.getWidth();
                int originalHeight = (int) slideShowImage.getHeight();
                int xPosition = 0;
                int yPosition = 0;
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                if (checkDuplicate(data, fileName, path) == 0) {
                    data.addSlide(fileName, path, caption, originalWidth, originalHeight, xPosition, yPosition);
                } else {
                    System.out.println("File Already exists");
                }
            }
        } catch (MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);

        }
    }

//    public void setSlideshowTitle() {
//        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
//        SlideshowCreatorWorkspace work = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
//        data.setSlideshowTitle(work.slideshowTitle.getText());
//        System.out.println(data.getSlideshowTitle());
//
//    }
    //SHOW DETAILS IN THE TEXT FIELDS AND SLIDERS
    public void setDetails() {
        SlideshowCreatorWorkspace work = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        String fileName = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getFileName();
        String caption = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getCaption();
        String path = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getPath();
        int originalWidth = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getOriginalHeight();
        int originalHeight = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getOriginalHeight();
        int xPosition = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getXPosition();
        int yPosition = work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getYPosition();
        work.fileNameTextField.setText(fileName);
        work.captionTextField.setText(caption);
        work.pathTextField.setText(path);
        work.originalHeightTextField.setText(Integer.toString(originalWidth));
        work.originalWidthTextField.setText(Integer.toString(originalHeight));
        work.currentHeightSlider.setValue(work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getCurrentHeight());
        work.currentWidthSlider.setValue(work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).getCurrentWidth());
        work.xPosition.setValue(xPosition);
        work.yPosition.setValue(yPosition);
    }

    //UPDATE DETAILS IN THE SLIDE
    public void updateDetails() {
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        SlideshowCreatorWorkspace work = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        int sliderWidth = (int) work.currentWidthSlider.getValue();
        int sliderHieght = (int) work.currentHeightSlider.getValue();
        String caption = work.captionTextField.getText();
        work.slideshowTitle.setText(data.getSlideshowTitle());
        work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).setCaption(caption);
        work.currentWidthSlider.setValue(work.currentWidthSlider.getValue());
        work.currentHeightSlider.setValue(work.currentHeightSlider.getValue());
        work.xPosition.setValue(work.xPosition.getValue());
        work.yPosition.setValue(work.yPosition.getValue());
        work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).setCurrentWidth(sliderWidth);
        work.slidesTableView.refresh();
        work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).setCurrentHeight(sliderHieght);
        work.slidesTableView.refresh();
        work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).setXPosition((int) work.xPosition.getValue());
        work.slidesTableView.refresh();
        work.slidesTableView.getItems().get(work.slidesTableView.getSelectionModel().getSelectedIndex()).setYPosition((int) work.yPosition.getValue());
        work.slidesTableView.refresh();
    }

    public void removeImage() {
        SlideshowCreatorWorkspace work = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        data.removeSlide(work.slidesTableView.getSelectionModel().getSelectedItem());

    }

    // THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
        File file = new File(imagePath);
        URL fileURL = file.toURI().toURL();
        Image image = new Image(fileURL.toExternalForm());
        return image;
    }

    private int checkDuplicate(SlideshowCreatorData slide, String fileName, String path) {
        int value = 0;
        for (int i = 0; i < slide.getSlides().size(); i++) {
            if ((slide.getSlides().get(i).getFileName().equals(fileName)) && (slide.getSlides().get(i).getPath().equals(path))) {
                value = 1;
            }

        }
        return value;
    }

}
